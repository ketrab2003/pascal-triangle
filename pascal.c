/*
Program prints n rows of Pascal's triangle
14.11.2019
Author: B. Krawisz
*/
#include <stdlib.h>
#include <stdio.h>

int get_value(int **tab, int x, int y){
    return tab[x][y];
}

void set_value(int ***tab, int x, int y, int val){
    (*tab)[x][y] = val;
}

void init_2d(int ***tab, int n){
    (*tab) = malloc(n*sizeof(int*));
    for(int i=0; i<n; ++i)
        (*tab)[i] = malloc((i+1)*sizeof(int));
}

int main(){
    int n;
    scanf("%d", &n);
    // Initialize new tab
    int **tab;
    init_2d(&tab, n);

    // Compute numbers
    for(int i=0; i<n; ++i){
        set_value(&tab, i, 0, 1);
        for(int j=1; j<i; ++j){
            int sum = get_value(tab, i-1, j) + get_value(tab, i-1, j-1);
            set_value(&tab, i, j, sum);
        }
        set_value(&tab, i, i, 1);
    }

    // Print result
    for(int i=0; i<n; ++i){
        for(int j=0; j<=i; ++j){
            printf("%d ", get_value(tab, i, j));
        }
        printf("\n");
    }
    return 0;
}