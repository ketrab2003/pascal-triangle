/*
Program prints n rows of Pascal's triangle
(Memory efficient version)
14.11.2019
Author: B. Krawisz
*/
#include <stdlib.h>
#include <stdio.h>

int main(){
    // Get data from user
    int n;
    scanf("%d", &n);

    int *tab = malloc(n*sizeof(int));

    for(int i=0; i<n; ++i){
        int *tmp_tab = malloc(n*sizeof(int));
        tmp_tab[0] = 1;
        for(int j=1; j<i; ++j){
            tmp_tab[j] = tab[j-1] + tab[j];
        }
        tmp_tab[i] = 1;
        free(tab);
        tab = tmp_tab;
        // Print line
        for(int j=0; j<=i; ++j)
            printf("%d ", tab[j]);
        printf("\n");
    }

    free(tab);

    return 0;
}
